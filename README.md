# Python script to query _ReM_ XML

The [Referenzkorpus Mittelhochdeutsch (1050–1350)](https://linguistics.rub.de/rem/) (_ReM_; Klein & Dipper 2016, Klein et al. 2016) is the foremost research corpus on the Middle High German language. Getting keyword-in-context (KWIC) data from its [ANNIS instance](https://annis.linguistics.rub.de/REM/) seems to be rather impossible, however, making it challenging to efficiently create example lists.

This is why I decided to write my own script to query the corpus's data in XML format as provided by the _ReM_ workgroup. Working on XML files is very slow compared to a proper SQL database, but it was straightforward to implement quickly and currently fits my needs. In addition, should the XML files be updated in the future, it'll be easy to drop in new data.

## Important files

These are the main files/directories contained in this repository:

| File name        | Purpose                                                                                                                                                              |
|------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| createmeta.py    | Creates `meta.json` file from XML sources in `./data`                                                                                                                |
| data/*.xml       | XML files that make up the corpus                                                                                                                                    |
| meta.json        | Contains all the metadata from the XML files outsourced to JSON format                                                                                               |
| requirements.txt | List of non-standard required packages                                                                                                                               |
| search.py        | The querying script                                                                                                                                                  |
| usersettings.py  | Contains settings users can edit to fit their needs (On first use, copy `usersettings.py.template` to `usersettings.py`. Edit the `.py` to your needs if necessary.) |

## Important notes

Crucially, note that you'll have to manually [download the XML files from the _ReM_ project website](https://linguistics.rub.de/rem/access/) and ideally unzip them directly to the `data` folder. If required, you can set the path to the directory containing the XML files manually by changing the value of the variable `XML_DATA_DIR` in `usersettings.py`. The unzipped XML files comprise over 1 GB in total.

Moreover, the program can't search for complex contexts where one annotation depends on another. For instance, it's *not* possible to search for _niht_ between 2 and 5 tokens to the right of _ne_.

In cases where the source contains more than one string per element, the pipe character ("|") divides the individual parts. This is the case especially for word forms consisting of two tokens, e.g. to document spacing between the morphemes of a lexical item in the manuscript. For instance, if the manuscript contains _ver iehen_ 'testify' (standardized spelling: _verjehen_), the program's output will print it as _ver|iehen_. The opposite case also exists, e.g. with _sagnich_ 'I tell' (standardized spelling: _sagen ich_). This compound word form analyzes as _say-1SG.IND.PRES=1SG.NOM_, so its annotations will be rendered as _VVFIN|PPER_, _Ind.Pres.Sg.1|Nom.Sg.1_, and _sagen|ich_.

The `createmeta.py` file basically exists to document the step of preparatory work where I pulled the metadata from each XML file and put it in `meta.json` for greater convenience. While running `createmeta.py` yields a useable `meta.json` file, the `time` field was originally left empty for a number of manuscripts (`--` in the XML sources, parsed as `None` in Python, `null` in JSON, respectively). For practical purposes those files wouldn't normally be included in a query narrowing down sources to half-centuries, which is something you may want to do. I thus manually filled in those empty `time` fields in `meta.json` based on the "ANNIS-Gruppe" classification in the [_ReM_ text overview](https://linguistics.rub.de/rem/corpus/texts.html) to make those files accessible.

## Usage

```bash
$ python3 search.py -h
usage: search.py [-h] [-a] [-b VAL VAL] [-c [TAG=ATTR ...]] [-j] [-l]
                 [-m META [META ...]]  [-s N]
                 [-t KEY=VAL [KEY=VAL ...]] [-x PATH]
                 KEY=VAL [KEY=VAL ...]

Search for things in the ReM corpus and return matches in context.
Search terms can be Python-compatible regular expressions
(https://docs.python.org/3/library/re.html). Output is to stdout in
CSV format by default.

positional arguments:
  KEY=VAL               The fields you want to search values in, e.g.
                        lemma=bèide infl=".*(Nom|Akk).Pl.st" searches
                        for the lemma 'bèide' where it's tagged for
                        NOM/ACC.PL.ST, i.e. the terms are logically
                        concatenated by AND.

options:
  -h, --help            show this help message and exit
  -a, --annotation      Return morphological annotation of match.
  -b VAL VAL, --bracket VAL VAL
                        Context bracket of match, e.g. 20 5 gives the
                        20 tokens before and 5 tokens after a match.
  -c [TAG=ATTR ...], --context [TAG=ATTR ...]
                        A pair of tag and attribute to retreive a
                        match's context from, e.g. tok_dipl=utf
                        (default).
  -j, --json            Print result as JSON array to stdout.
                        Mutually exclusive with both XLSX and CSV
                        output.
  -l, --location        Print location information on match (depending
                        on coding in document, e.g. folio, line; verse).
  -m META [META ...], --meta META [META ...]
                        Also return the specified metadata items for
                        each match, e.g. text time language-region.
                        See meta.json for other field names.
  -s N, --sample N      Collect a random sample from the list of
                        results. Either define a decimal fraction to
                        specify a percentage or define an integer
                        for a definitive number, e.g. 0.125 or 1000.
  -t KEY=VAL [KEY=VAL ...], --texts KEY=VAL [KEY=VAL ...]
                        Filter the list of text sources by matching
                        conditions, e.g. language-type=oberdeutsch
                        time="^(12,2|13)" attempts to include Upper
                        German texts from the 2nd half of the 12th or
                        from the 13th century. See meta.json for other
                        field names.
  -x PATH, --xlsx PATH  Save output in XLSX format, e.g.
                        "/home/user/Documents/out.xlsx". Mutually
                        exclusive with both JSON and CSV output.
```

Example of a call to `search.py`:

```bash
$ python3 search.py \
  lemma=bèide infl=".*(Nom|Akk).Pl.st" \
  -a \
  -b 35 5 \
  -l \
  -m time text library library-shelfmark \
  -t language-type=oberdeutsch time="(12,2|13)" \
  > ~/out.csv \
  2> ~/out.log
```

* This queries queries for tokens where the `lemma` is *bèide* and `infl` is simultaneously coded for NOM.PL.ST or ACC.PL.ST.
* Morphological annotations consisting of lemmatization and POS tags are included (`-a`).
* The context window around the match is set to 35 tokens to the left and 5 to the right (`-b`).
* Location information (folio/line in the transcribed manuscript or verse in the referenced edition) is included (`-l`).
* For metadata, `time` (e.g. 13,2 corresponds to the 2nd half of the 13th century), `text` (e.g. Hartmann von Aue: _Iwein_), `library` (e.g. Munich, Bavarian State Lib.), and `library-shelfmark` (e.g. Cod. Pal. germ. 848) are included in the listing (`-m`).
* Source texts are narrowed down by `language-type` and `time`, here selected as Upper German texts of the second half of the 12th or the 13th century (`-t`).
* Since `-c` is not set, the diplomatic transcription of each token in Unicode format is returned for context.
* Since `-j` is not set, output is to `stdout` in CSV format.
* `stdout` is redirected to `out.csv` in the user's home directory.
* `stderr` is redirected to `out.log` in the user's home directory.

## Installing dependencies

Non-standard dependencies are listed in `requirements.txt`. It's recommended to use a [virtual environment](https://docs.python.org/3/library/venv.html) for this purpose. You can install the extra packages by executing

```bash
$ pip install -r requirements.txt
```

## References
* Klein, T. & S. Dipper (2016). _Handbuch zum Referenzkorpus Mittelhochdeutsch_ (Bochumer linguistische Arbeitsberichte 19). Bochum: Ruhr-Universität Bochum. [https://linguistics.rub.de/forschung/arbeitsberichte/19.pdf](https://linguistics.rub.de/forschung/arbeitsberichte/19.pdf) (accessed on 2024-02-11).
* Klein, T., K.-P. Wegera, S. Dipper & C. Wich-Reif (eds., 2016). _Referenzkorpus Mittelhochdeutsch (1050–1350)_. Version 1.0. Ruhr-Universität Bochum & Rheinische Friedrich-Wilhelms-Universität Bonn. [https://linguistics.rub.de/rem/](https://linguistics.rub.de/rem/) (accessed on 2024-02-11).

---

Copyright © 2024, Carsten Becker (carsten.becker[at]hu-berlin[dot]de), licensed under GPL 3.0 (_ReM_ data originally published under CC BY-SA 4.0)
