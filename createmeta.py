#! /usr/bin/env python3

import csv
import json
import sys
import usersettings
from bs4 import BeautifulSoup as bs
from os import listdir
from os.path import abspath, isfile, join


# Shortcut to handle stuff like ~/ on UNIXoid systems
def myabspath(path=""):
	return abspath(expanduser(expandvars(path)))


# Initialize container file in JSON format
with open("meta.json", "w") as fout:

	# Set up container to hold all records
	container = []

	# Open XML files in ./data
	files = [f for f in listdir(myabspath(usersettings.XML_DATA_DIR))
		if isfile(join(myabspath(usersettings.XML_DATA_DIR), f)) and re.match(r".*\.xml", f)]

	# Read <header> of all files into dictionary
	for i,file in enumerate(files):

		# Read and evaluate contents of individual XML file
		with open(join("data", file), "r") as fin:
			soup = bs(fin, "xml")
			header = soup.header

			# Get list of tags in <header>, dispose of None
			ht = list(filter(lambda l: l != None,
					[x.name for x in header.contents]))

			# Make dictionary headerelem => value based on list ht
			# Return literal "-" as None
			metadata = {}
			for k in ht:
				v = header.find(k).text
				v = v.replace('"', "'")
				metadata[k] = None if v == "-" or v == "_" else v

			# Add file name to dictionary
			metadata["file"] = file

		# Append record to container
		container.append(metadata)

	# Write record list to file
	json.dump(container, fout, indent=4)
