#! /usr/bin/env python3

import argparse
import json
import random
import re
import sys
import usersettings
import xlsxwriter
from bs4 import BeautifulSoup
from os import listdir
from os.path import abspath, expanduser, expandvars, isfile, join


# Define class StoreDictKeyPair to be able to treat argparse arguments as dict
# cf. https://stackoverflow.com/a/42355279
class StoreDictKeyPair(argparse.Action):
	def __init__(self, option_strings, dest, nargs=None, **kwargs):
		self._nargs = nargs
		super(StoreDictKeyPair, self).__init__(option_strings, dest, nargs=nargs, **kwargs)
	def __call__(self, parser, namespace, values, option_string=None):
		my_dict = {}
		for kv in values:
			key, val = kv.split("=")
			my_dict[key] = val
		setattr(namespace, self.dest, my_dict)


# Read meta.json into dictionary
with open("meta.json", "r") as fin:
	metadata = json.load(fin)


# Shortcut to handle stuff like ~/ or $HOME on UNIXoid systems
def myabspath(path=""):
	return abspath(expanduser(expandvars(path)))


# Get longest filename in data directory
longest = 0
for fname in listdir(myabspath(usersettings.XML_DATA_DIR)):
	if len(fname) > longest:
		longest = len(fname)


# Return list of files
# search: list of dictionary pairs {"key1": "value1", "key2": "value2"}
def filterfiles(search={}):

	# Set up container to include all results
	result = []

	# Search through records in metadata
	for record in metadata:

		# Set up flag
		# All criteria need to be true simultaneously (x AND y)
		flag = True

		# Only consider the subset of keys common to both lists
		for key in list(search.keys() & record.keys()):
			
			# Check if flag has been set to false already
			if not flag:
				continue

			# regex-match items in search against metadata records
			# Set flag to False if field's value is None or doesn't match
			compsearchkey = re.compile(search[key])
			if record[key] == None:
				flag = False
			elif not compsearchkey.match(record[key]):
				flag = False

		# If all search terms apply to current record, add file name to list
		if flag:
			result.append(record["file"])

	return result


# Search for element in XML file according to criteria
# search: list of dictionary pairs {"key1": "value1", "key2": "value2"}
# lst:    ResultSet of tags/tokens (aggregated by soup.find_all("tag")),
def searchinlist(
		search={},
		lst=[]
	):
	
	# Set up container to include all results
	result = []

	# Loop through list items
	for item in lst:

		# Set up flag
		# All criteria need to be true simultaneously (x AND y)
		flag = True

		# Loop through search criteria
		for key, val in search.items():

			# Check if flag has already been set to false
			if not flag:
				continue

			# Set flag to False when a criterion doesn't apply
			if not item.find_all(key, tag=re.compile(val)):
				flag = False

		# If flag is still True, add item to results
		if flag:
			result.append(item)

	return result


# Extract a match's token ID as an integer, <tok_anno … id="t123_m1" …>
# item: list of Tag elements in match
def getitemid(item):
	return int(re.search(r"t(\d+)", item["id"])[1])


# Format output of tag/attribute search
# Handles cases of 1 or more tags of the same kind in the match consistently
def gettagattr(match, tag, attr):
	return "|".join(i[attr] for i in match.find_all(tag))


# Get context of match from tree, by default in KWIC format
#
# matchitem: list containing the single matched tag/token
# matchlist: ResultSet containing complete list of matched token tags
# tag:       XML tag to evaluate
# attr:      XML attribute to evaluate
# left:      integer of tags to the left of the match
# right:     integer of tags to the right of the match
#
# Note: token IDs start from 1, not 0
def annoincontext(
		matchitem,
		matchlist,
		left=0,
		right=0,
		tag="tok_dipl",
		attr="utf",
	):
	
	# Get match's id
	mid = getitemid(matchitem)

	# Set up container to include all results
	result = []

	# Loop through elements in match's parent, add the transcription in Unicode
	# format of each token in the specified range to the container as a string.
	# (There can be more than one <tok_dipl> per <token>!)
	for item in matchlist:
		if getitemid(item) in range(mid - left, mid + right + 1):
			if not item.find(tag):
				result.append("Ø")
			elif getitemid(item) == mid:
				result.append("{}{}{}".format(
					usersettings.LEFTMARK,
					gettagattr(item, tag, attr),
					usersettings.RIGHTMARK
				))
			else:
				result.append(gettagattr(item, tag, attr))

	# Return tokens as string joined up by spaces
	return " ".join(result)


# Get location information (e.g. folio/line, verse) for match
# <line … range="t123_d1..t234_d2" loc="0a,123" />
# item: Tag element containing match
def getitemloc(item, linemd):
	
	# Loop over linemd
	for l in linemd:

		# Parse range field for token IDs
		t = re.findall(r"t(\d+)", l["range"])

		# This includes at least 1 token, so we need to check how many!
		# Matches are returned by re.findall() as a list
		if len(t) == 2:
			rfrom, rto = [int(i) for i in t]
			r = range(rfrom, rto + 1)

			if getitemid(item) in r:
				return l["loc"]

		elif len(t) == 1:
			rfrom = int(t[0])
			r = range(rfrom + 1)

			if getitemid(item) in r:
				return l["loc"]

		else:
			return None


# Main function
def main(argv=None):
	if argv is None:
		argv = sys.argv[1:]

	# Parser for command line options
	parser = argparse.ArgumentParser(
		description="""Search for things in the ReM corpus and return matches
			in context. Search terms can be Python-compatible regular
			expressions (https://docs.python.org/3/library/re.html). Output is
			to stdout in CSV format by default."""
	)
	
	parser.add_argument(
		"-a", "--annotation",
		action="store_true",
		default=False,
		help="""Return morphological annotation of match."""
	)
	parser.add_argument(
		"-b", "--bracket",
		nargs=2,
		metavar="VAL",
		type=int,
		help="""Context bracket of match,
			e.g. 20 5 gives the 20 tokens before and 5 tokens after a match."""
	)
	parser.add_argument(
		"-c", "--context",
		action=StoreDictKeyPair,
		nargs="*",
		metavar="TAG=ATTR",
		default={"tok_dipl":"utf"},
		help="""A pair of tag and attribute to retreive a match's context from,
			e.g. tok_dipl=utf (default)."""
	)
	parser.add_argument(
		"-j", "--json",
		action="store_true",
		default=False,
		help="""Print result as JSON array to stdout.
			Mutually exclusive with both XLSX and CSV output."""
	)
	parser.add_argument(
		"-l", "--location",
		action="store_true",
		default=False,
		help="""Print location information on match (depending on coding in
			document, e.g. folio, line; verse)."""
	)
	parser.add_argument(
		"-m", "--meta",
		nargs="+",
		help="""Also return the specified metadata items for each match, e.g.
			text time language-region. See meta.json for other field names."""
	)
	parser.add_argument(
		"-s", "--sample",
		type=float,
		metavar="N",
		default=False,
		help="""Collect a random sample from the list of results. Either define
			a decimal fraction to specify a percentage or define an integer for
			a definitive number, e.g. 0.125 or 1000."""
	)
	parser.add_argument(
		"-t", "--texts",
		action=StoreDictKeyPair,
		nargs="+",
		metavar="KEY=VAL",
		help="""Filter the list of text sources by matching conditions,		
			e.g. language-type=oberdeutsch time="^(12,2|13)" attempts to include
			Upper German texts from the 2nd half of the 12th or from the 13th
			century. See meta.json for other field names."""
	)
	parser.add_argument(
		"-x", "--xlsx",
		type=str,
		metavar="PATH",
		default=False,
		help="""Save output in XLSX format,
			e.g. "/home/user/Documents/out.xlsx."
			Mutually exclusive with both JSON and CSV output."""
	)
	parser.add_argument(
		"search",
		action=StoreDictKeyPair,
		nargs="+",
		metavar="KEY=VAL",
		help="""The fields you want to search values in,
			e.g. lemma=bèide infl=".*(Nom|Akk).Pl.st" searches for the lemma
			"bèide" where it's tagged for NOM/ACC.PL.ST, i.e. the terms are
			logically concatenated by AND."""
	)

	args = parser.parse_args(argv)

	# Set up container to include all results
	result = []
	totaltoks = 0

	# Prepare file list of texts to consider as source. If nothing is specified,
	# return any.
	if args.texts:
		texts = filterfiles(args.texts)
	else:
		texts = filterfiles()

	# Print args for documentation in JSON format for legibility, but with
	# Unicode characters not escaped (also for legibility)
	sys.stderr.write("Arguments passed: {}\n\n".format(
		json.dumps(vars(args), indent=4, ensure_ascii=False)))

	# Loop through text files
	for i, text in enumerate(texts):
		
		# Set up container for to include all matches
		matches = []

		# Prepare soup
		with open(join(myabspath(usersettings.XML_DATA_DIR), text), "r") as f:
			soup = BeautifulSoup(f, "xml")

		# Return list of matches for the current text
		toks = soup.find_all("token")
		totaltoks += len(toks)
		m = searchinlist(args.search, toks)

		# Next element in loop if no match
		if not m:
			sys.stderr.write(
				"file: {:<{}s} ({:>3d}/{:<3d}), matches: {:>5d}/{:<5d} (skipping)".format(
				text, longest, i+1, len(texts), len(m), len(toks)) + "\n")
			continue

		# Read in <line> metadata
		linemd = soup.find_all("line")

		# Add items to general list of matches
		for item in m:
			matches.append(item)

		# Print some information to screen (only!) to keep track of progress
		sys.stderr.write(
			"file: {:<{}s} ({:>3d}/{:<3d}), matches: {:>5d}/{:<5d}".format(
			text, longest, i+1, len(texts), len(matches), len(toks)) + "\n")

		# Loop through list of matches
		for match in matches:

			# Set up container to include the current record
			record = {}

			# Get meta information on current match
			tag, attr = list(args.context.items())[0]
			record["textid"] = soup.find("text")["id"]
			record["tokenid"] = getitemid(match)

			# Get keyword for match
			record["keyword"] = gettagattr(match, tag, attr)

			# If grammatical annotation is activated, also include
			# morphological annotation and lemma
			if args.annotation:
				record["norm"]       = gettagattr(match, "norm",  "tag")
				record["pos"]        = gettagattr(match, "pos",   "tag")
				record["annotation"] = gettagattr(match, "infl",  "tag")
				record["lemma"]      = gettagattr(match, "lemma", "tag")

			# Get context of match
			record["context"] = annoincontext(
				match,
				toks,
				args.bracket[0],
				args.bracket[1],
				tag,
				attr
			)

			# Include metadata fields if any are specified
			if args.meta:
				
				# Pick dataset for current file from metadata records
				# cf. https://stackoverflow.com/a/7079297
				mdset = next(
					(item for item in metadata if item["file"] == text), None)

				# Loop through list of metadata fields
				for field in args.meta:
					record[field] = mdset[field]

			# Add locator if given
			if args.location:
				record["loc"] = getitemloc(match, linemd)

			# Add complete record to result set
			result.append(record)

	# Write sum of matches and tokens to screen only
	sys.stderr.write("matches total: {:>7d}\n".format(len(result)))
	sys.stderr.write("tokens total:  {:>7d}\n".format(totaltoks))

	# Random sample
	if args.sample:

		# If some non-zero (i.e. non-false) value was submitted
		if args.sample >= 1:

			# Definitive number of elements from list
			# This is error-tolerant with regard to positive float values
			n = round(args.sample)

		else:
			# Percentage of elements from list, rounded to closest integer
			n = round(args.sample * len(result))
			
		try:
			# Overwrite result with random draw
			result = random.sample(result, n)

			# Sort the list by all keys
			result = sorted(result, key=lambda x: tuple(x[k] for k in result[0].keys()))

			# Write information on resulting sample size to screen only
			sys.stderr.write("sample size:   {:>7d}\n".format(len(result)))

		except ValueError as e:
			sys.exit("{}\n".format(e))
		except TypeError as e:
			sys.exit("{}\n".format(e))

	# Format output
	if args.json:
		# Output as JSON array
		return "{}\n".format(json.dumps(result, indent=4))

	elif args.xlsx:
		# Write to XLSX format for convenience, because importing CSV to the
		# Big E always seems unnecessarily complicated
		try:
			# Create XLSX file and add worksheet to dump data in
			outpath = myabspath(args.xlsx)
			workbook = xlsxwriter.Workbook(outpath)
			worksheet = workbook.add_worksheet("Query result")

		except xlsxwriter.exceptions.FileCreateError as e:
			sys.exit("ERROR: {}".format(e))

		# Set up columns of the XLSX table
		try:
			keys = result[0].keys()
		except IndexError as e:
			sys.exit("ERROR: {}".format(e))

		# Write keys to first line, start at coordinates (0;0)
		r, c = 0, 0
		for item in keys:
			worksheet.write(r, c, str(item).strip())
			c += 1

		# Loop through result set, start at coordinates (1;0)
		r = 1
		for record in result:

			# Set up container to hold the values
			line = []

			# For each key, get corresponding value
			for key in keys:
				line.append(record[key])

			# Write out columns in current row in XLSX format,
			# start at coordinates (r;0)
			c = 0
			for item in line:
				worksheet.write(r, c, str(item).strip())
				c += 1

			# Increase row counter
			r += 1

		# Close workbook
		workbook.close()
		return "Query result saved to {}\n".format(outpath)

	else:
		# Set up columns of the CSV table
		try:
			keys = result[0].keys()
			rows = []
			rows.append(keys)
		except IndexError as e:
			sys.exit("ERROR: {}".format(e))
		
		# Loop through result set
		for record in result:
			
			# Set up container to hold the values
			line = []

			# For each key, get corresponding value
			for key in keys:
				line.append(record[key])

			# Put values in list of rows
			rows.append(line)

		# Write out in CSV-compatible format
		return "\n".join([
			",".join([
				'"{}"'.format(str(i).strip()) for i in line
			]) for line in rows
		]) + "\n"


# Execute the program
if __name__ == "__main__":

	# Returning output to stdout
	sys.stdout.write(main())

	# Close program
	sys.exit()
